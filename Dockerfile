FROM caddy:builder-alpine AS builder
RUN xcaddy build 

FROM php:8.1-fpm-alpine
LABEL maintainer="Jay Williams <course@jaywilliams.me>"
COPY --from=builder /usr/bin/caddy /usr/bin/caddy

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

# persistent dependencies
# JAY: removed ffmpeg
# Jay: removed icu-dev from 3rd apk add
# Jay: removed intl from docker-php-ext-install
# docker-php-ext-configure intl
RUN set -eux; \
	apk add --no-cache ghostscript imagemagick supervisor git sudo; \
	apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv; \
	apk add --no-cache --virtual .build-deps $PHPIZE_DEPS freetype-dev imagemagick-dev libjpeg-turbo-dev libpng-dev libzip-dev libwebp-dev; \
	docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp; \
	docker-php-ext-install bcmath exif gd mysqli zip pdo pdo_mysql; \
	pecl install imagick-3.6.0 redis apcu; \
	docker-php-ext-enable imagick redis apcu; \
	rm -r /tmp/pear; \
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-network --virtual .wordpress-phpexts-rundeps $runDeps; \
	apk del --no-network .build-deps; \
# Run tests
	! { ldd "$extDir"/*.so | grep 'not found'; }; \
	err="$(php --version 3>&1 1>&2 2>&3)"; \
	[ -z "$err" ]; \
	docker-php-ext-enable opcache; \
	{ \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini; \
	{ \
# https://www.php.net/manual/en/errorfunc.constants.php
# https://github.com/docker-library/wordpress/issues/420#issuecomment-517839670
		echo 'error_reporting = E_ERROR | E_WARNING | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING | E_RECOVERABLE_ERROR'; \
		echo 'display_errors = Off'; \
		echo 'display_startup_errors = Off'; \
		echo 'log_errors = On'; \
		echo 'error_log = /dev/stderr'; \
		echo 'log_errors_max_len = 1024'; \
		echo 'ignore_repeated_errors = On'; \
		echo 'ignore_repeated_source = Off'; \
		echo 'html_errors = Off'; \
	} > /usr/local/etc/php/conf.d/error-logging.ini

#### START OWN

RUN addgroup -g 1000 caddy && adduser caddy -u 1000 -G caddy -D -s /bin/sh 
RUN rm -rf /usr/local/etc/php-fpm.d/*

WORKDIR /app/

VOLUME ["/etc/caddy"]

COPY ./files/supervisord.conf /etc/
COPY ./supervisord /etc/supervisord/
COPY ./scripts/entrypoint.sh /app/
RUN chmod +x /app/entrypoint.sh

EXPOSE 80 443

ENTRYPOINT ["sh", "/app/entrypoint.sh"]
